#
# Debian IceWM Novnc Dockerfile
#
# Pull base image.
FROM debian

# Setup enviroment variables
ENV DEBIAN_FRONTEND noninteractive

ARG WEBUSERNAME=automat
ENV WEBUSERNAME ${WEBUSERNAME}

# Update the package manager and upgrade the system
# #################################################
RUN apt-get update && \
apt-get upgrade -y && \
apt-get update

RUN apt-get -y install net-tools passwd bzip2 sudo wget vim-tiny
RUN apt-get -y install tini supervisor
RUN apt-get -y install openssh-server openssh-client
RUN apt-get -y install locales locales-all
RUN apt-get -y install git tcl tk make
RUN mkdir -p /var/run/sshd && sed -i "s/UsePrivilegeSeparation.*/UsePrivilegeSeparation no/g" /etc/ssh/sshd_config \
 && sed -i 's/PermitRootLogin without-password/PermitRootLogin yes/' /etc/ssh/sshd_config
RUN ssh-keygen -A

# Set the locale
ENV LC_ALL en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8


# Install icewm and tightvnc server.
# #################################################
RUN apt-get -y install xorg xauth xinit xfonts-base
RUN apt-get -y install xterm firefox-esr 
RUN apt-get -y install icewm
RUN apt-get -y install tigervnc-standalone-server tigervnc-common
RUN apt-get clean all
ENV GEOMETRY 1320x720

# install and setup noVNC
# #################################################
RUN apt-get -y install novnc 
RUN apt-get -y install websockify
RUN ln -s /usr/share/novnc/vnc_lite.html /usr/share/novnc/index.html


# Setup Supervisord and conf files
# #################################################
ADD ./supervisor/ /etc/supervisor/
RUN echo 'export DISPLAY=:1' >> /root/.bashrc

# Set up User (${WEBUSERNAME})
# #################################################
RUN useradd -s /bin/bash -m -b /home ${WEBUSERNAME}
RUN touch /home/${WEBUSERNAME}/.Xauthority
RUN chmod go-rwx /home/${WEBUSERNAME}/.Xauthority
RUN chown -R ${WEBUSERNAME}:${WEBUSERNAME} /home/${WEBUSERNAME}
RUN sed -i "s/webusername/${WEBUSERNAME}/g" /etc/supervisor/conf.d/icewm-session.conf
RUN sed -i "s/webusername/${WEBUSERNAME}/g" /etc/supervisor/conf.d/Xvnc.conf

# Set up User sshadmin
# #################################################
RUN useradd -s /bin/bash -m -b /home sshadmin
RUN echo "sshadmin  ALL=(ALL) NOPASSWD:ALL" | sudo tee /etc/sudoers.d/sshadmin
RUN mkdir /home/sshadmin/.ssh
ADD authorized_keys /home/sshadmin/.ssh/authorized_keys
RUN chown -R sshadmin:sshadmin /home/sshadmin/.ssh
RUN chmod -R go-rwx /home/sshadmin/.ssh
RUN echo 'export DISPLAY=:1' >> /home/sshadmin/.bashrc

# Finalize installation and default command
# #################################################
ADD run.sh /root/run.sh
RUN chmod +x /root/*.sh
RUN mkdir /tmp/.X11-unix
RUN chmod 1777 /tmp/.X11-unix

# Expose ports.
#  novnc
#  sshd
EXPOSE 8080
EXPOSE 22

# Define default command
#
CMD ["/root/run.sh"]
