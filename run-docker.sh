#! /bin/bash
IMAGE=debian-icewm-novnc
WEBPORT=1080
SSHPORT=1022
GEOMETRY=1360x760
# Stop current running container
docker stop $(docker container ls -a | grep -i $IMAGE | awk '{print $1}') 2>/dev/null

#Start new container
docker run -d -p $WEBPORT:8080 -p $SSHPORT:22 -e GEOMETRY=$GEOMETRY -h $IMAGE $IMAGE:latest
