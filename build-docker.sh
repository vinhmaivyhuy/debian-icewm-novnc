#!/bin/bash
IMAGE=debian-icewm-novnc
docker stop $(docker container ls -a | grep -i $IMAGE | awk '{print $1}') 2>/dev/null
docker build -t $IMAGE --build-arg WEBUSERNAME=nguyenv .
docker tag $IMAGE:latest 600703138910.dkr.ecr.us-east-1.amazonaws.com/$IMAGE:latest
docker image prune --force
